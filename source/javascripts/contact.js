(function() {
  var switchFormsBool = false;

  var switchButtons = document.getElementsByClassName('switch-forms');
  var securityNoticesForm = document.getElementById('security-notices-form');
  var newsletterForm = document.getElementById('newsletter-form');

  securityNoticesForm.style.display = 'none';
  securityNoticesForm.style.transform = 'scale(1.2)';
  securityNoticesForm.style.opacity = '0';

  function switchForms() {
    switchFormsBool = !switchFormsBool;
    if (switchFormsBool) {
      securityNoticesForm.style.display = 'flex';
      newsletterForm.style.transform = 'scale(.8)';
      newsletterForm.style.opacity = '0';
      setTimeout(function() {
        securityNoticesForm.style.transform = 'scale(1)';
        securityNoticesForm.style.opacity = '1';
      }, 10);
    } else {
      securityNoticesForm.style.transform = 'scale(1.2)';
      securityNoticesForm.style.opacity = '0';
      newsletterForm.style.transform = 'scale(1)';
      newsletterForm.style.opacity = '1';
      setTimeout(function() {
        securityNoticesForm.style.display = 'none';
      }, 400);
    }
  }

  for (var i = 0; i < switchButtons.length; i++) {
    switchButtons[i].addEventListener('click', switchForms);
  }
})();
