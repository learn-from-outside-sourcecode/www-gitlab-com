---
layout: markdown_page
title: GitLab for education
---

GitLab is being widely used by educational institutions around the world.

# Educational pricing


GitLab’s educational pricing policy applies to GitLab.com Gold plans and GitLab Ultimate self-hosted instances for educational use.

Educational pricing is for educational institutions. This means that only staff are included in the "user count," and students can be added free of charge after a purchase for staff users.   

Any institution whose purposes directly relate to learning, teaching, and/or training may qualify. Educational purposes do not include commercial, professional, or any other for-profit purposes.   

To apply, send an email to [education@gitlab.com](mailto:education@gitlab.com) from your institution's email address with details about your institution and program. Once your application has been approved, we will send your license code.
