---
layout: markdown_page
title: "Content"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Content Hack Day

[Content Hack Day](https://gitlab.com/gitlab-com/content-hack-day/blob/master/README.md) takes place quarterly, on the second Friday of the quarter. It's a day dedicated to writing posts for the [GitLab blog](/blog), and all team members are encouraged to take part.

### Upcoming events

- July 13, 2018
- October 12, 2018
- January 11, 2019

These can also be found on the GitLab Availability calendar.

### Process

#### Getting started

Team members who want to take part can open a new issue in the [Content Hack Day project](https://gitlab.com/gitlab-com/content-hack-day/issues), pick up an existing issue, or pick up an issue from the [blog posts project](https://gitlab.com/gitlab-com/blog-posts/issues).

**If a team member wants to tackle an issue from the blog posts project, they should open a new issue in the Content Hack Day project and mark it as related to the existing blog posts project issue.**

Team members are welcome to brainstorm post ideas in the #content-hack-day channel on Slack, or join the open Zoom call with content team members to discuss. The Zoom call link will be announced and pinned in the Slack channel.

Please also refer to the [blog handbook](/handbook/marketing/blog/) for guidance, and feel free to submit a merge request to improve it!

#### Submitting

In order to be eligible for an individual or team prize, participants must submit a complete draft (merge request or Google doc in the case of team members unfamiliar with creating a blog merge request) by the end of the day on the Friday following the Hack Day. Please assign to @rebecca for review.

The post should be reviewed by a peer before assigning to Rebecca.

#### Publishing

Your post will be reviewed as soon as possible, but please be prepared for some delay! The point of the Hack Day is so that you have a dedicated day for writing and working on posts with your team, but we will stagger publishing your posts throughout the rest of the quarter.

There may be some feedback for you to address after your review; this is a collaborative process and it should hopefully result in the best possible version of your blog post.

A member of the [content team](/handbook/marketing/marketing-sales-development/content/#roles) will let you know when to expect to see your post live, so you can go forth and share it with your networks.

#### Incentives and rewards

**Individual prize**

TBC shortly

**Team prize**

TBC shortly

**Random giveaways**

Swag will be given away randomly throughout the day. Anyone who has joined the Zoom call or been active in the project or the Slack channel is eligible. Names will be entered into a random picker to decide on winners.

**Snack stipend**

Team members may expense up to US $15 for snacks and drinks to keep you going throughout the day. Use the category `Meals - company provided`.
