---
layout: markdown_page
title: "Security Products Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Security Products Team

The security products team is responsible for the SAST and DAST features in the GitLab platform.
